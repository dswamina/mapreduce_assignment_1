import MapReduce
import sys
import re
from collections import Counter
#import Collections
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
	i=record[0]
	j=record[1]
	val=record[2]
	for k in range(5):
		mr.emit_intermediate((i,k),('A',j,val))
		mr.emit_intermediate((k,j),('B',i,val))

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts


	a_elm=[]
	b_elm=[]
	total=0

	for v in list_of_values:

		if v[0]=="A":
			a_elm.append((v[1],v[2]))
		elif v[0]=="B":
			b_elm.append((v[1],v[2]))
		for a in a_elm:
		for b in b_elm:
			if a[0]==b[0]:
				total+=a[1]*b[1]
	mr.emit((key[0],key[1],total))


# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
