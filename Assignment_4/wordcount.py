import MapReduce
import sys
import re
from collections import Counter
#import Collections
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
	#record = (i,j,value)
	i=record[0]
	j=record[1]
	val=record[2]
	mr.emit_intermediate(j,('A',i,val))
	j = record[0]
	k = record[1]
	mr.emit_intermediate(j,('B',k,val))

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts


	a_elm=[]
	b_elm=[]
	for v in list_of_values:
		#key is j
		if v[0]=="A":
			a_elm.append((v[1],v[2]))
		elif v[0]=="B":
			b_elm.append((v[1],v[2]))
	for (i,a_ij) in a_elm:
		for (k,b_jk) in b_elm:
			nn = [int(i),int(k),int(a_ij*b_jk)]
			mr.emit(nn)
	


# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
