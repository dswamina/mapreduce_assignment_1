import MapReduce
import sys
import re
from collections import Counter
#import Collections
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
		key = str(record[0])+ " " + str(record[1])
		mr.emit_intermediate(key,record[2])


def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
		total=0
		for i in list_of_values:
			total+=i
		mr.emit((key,total))


# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
