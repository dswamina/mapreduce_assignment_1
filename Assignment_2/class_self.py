""" Miracle! """
class MyClass:
    """A simple example class"""
    def __init__(self):
        print "initialize method"

    i = 12345
    def f(self):
        return 'hello world'
x = MyClass()
print x.i
print x.f()
print x.__doc__
print __doc__
