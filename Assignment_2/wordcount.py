import MapReduce
import sys
import re
from collections import Counter
#import Collections
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents

    for n1,v in enumerate(record) :
        for n2 in record[n1+1:]:
            key = str(v) + " "+ str(n2)
            mr.emit_intermediate(key,1)
def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts

    word = str(key)
    total = len(list_of_values)
    if total >= 100 :
        ss = word.split()
        mr.emit([int(ss[0]) , int(ss[1])])

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
