import MapReduce
import sys
import re
from collections import Counter
#import Collections
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    value = value.lower()
    if value.isalnum:
        words = value.split()
        for w in words:
            if w.isalnum():
                mr.emit_intermediate(w, key)

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    total = 0
    word = key
    unique_occurences = set(list_of_values)
    nof = len(unique_occurences)
    dict_doc = {}
    dict_doc = Counter(list_of_values)
    red_list = []
    for k,value in dict_doc.items():
        red_list.append([k,value])
    #Collections.sort(red_list)
    red_list.sort()
    mr.emit((word,nof,red_list))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
